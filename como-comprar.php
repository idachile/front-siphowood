<?php include('header.php'); ?>
    <main>
		<section class="horizon__search bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-9 gridle-gr-12@medium">
						<?php include('partials/breadcrumbs.php');?>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<?php include('partials/searchbar.php');?>
					</div>
				</div>
			</div>
		</section>

		<section class="horizon horizon__inner bg-lines">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-12 gridle-gr-12@medium">
						<article class="common-box--horizontal common-box--featured">
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="http://placehold.it/752x423">
								</a>
							</figure>
							<div class="common-box__body">
								<h1 class="title">
									¿Cómo comprar?
								</h1>
										
								<p class="common-box--featured">
									Dadipiscing elit. Fusce accumsan risus et hendrerit finibus. Quisque eleifend sagittis mauris eget acaracteres Lorem ipsum dolor sit amet, consectetur adipiscing Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce accumsan risus et hendrerit finibus. Quisque eleifend sagittis mauris eget 
								</p>
							</div>
						</article>
					</div>
				</div>
				
				<div class="gridle-row">
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article class="common-box--featured">
							 <h2 class="main-title--tiny"><strong>Paso 1</strong></h2>

							<figure class="common-box__figure">
								<img src="http://placehold.it/272x153">
							</figure>
							<div class="common-box__body">

								<p class="common-box__excerpt">
									Quisque eleifend sagittis mauris eget iaculis. In tempus nunc arcu, sed semper metus facilisis sit amet.Lorem ipsum dolor sit amet, consectetur adipiscing Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce accumsan risus et hendrerit finibus. Quisque eleifend sagittis mauris eget iaculis.
								</p>

							</div>
						</article>
					</div>
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article class="common-box--featured">
							<h2 class="main-title--tiny"><strong>Paso 2</strong></h2>

							<figure class="common-box__figure">
								<img src="http://placehold.it/272x153">
							</figure>
							<div class="common-box__body">

								<p class="common-box__excerpt">
									Quisque eleifend sagittis mauris eget iaculis. In tempus nunc arcu, sed semper metus facilisis sit lorem ipsum dolor.
								</p>

							</div>
						</article>
					</div>
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article class="common-box--featured">
							<h2 class="main-title--tiny"><strong>Paso 3</strong></h2>

							<figure class="common-box__figure">
								<img src="http://placehold.it/272x153">
							</figure>
							<div class="common-box__body">

								<p class="common-box__excerpt">
									Quisque eleifend sagittis mauris eget iaculis. In tempus nunc arcu, sed semper metus facilisis sit lorem ipsum dolor sit amet, consectetur adipiscing Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce accumsan risus et hendrerit finibus. Quisque eleifend sagittis mauris eget iaculis..
								</p>

							</div>
						</article>
					</div>
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article class="common-box--featured">
							<h2 class="main-title--tiny"><strong>Paso 4</strong></h2>

							<figure class="common-box__figure">
								<img src="http://placehold.it/272x153">
							</figure>
							<div class="common-box__body">

								<p class="common-box__excerpt">
									Quisque eleifend sagittis mauris eget iaculis. In tempus nunc arcu, sed semper metus facilisis sit lorem ipsum dolor sit amet, consectetur adipiscing Lorem ipsum dolor sit amet, conse
								</p>

							</div>
						</article>
					</div>
				</div>
			</div>
		</section>

		<section class="horizon horizon_inner horizon--extra bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-9 gridle-gr-12@medium">
						<div class="gridle-row">
							<div class="gridle-gr-9">
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="http://placehold.it/656x369">
									</a>
								</figure>
							</div>
						</div>
						<p class="common-box__excerpt">
							Quisque eleifend sagittis mauris eget iaculis. In tempus nunc arcu, sed semper metus facilisis sit amet.Lorem ipsum dolor sit amet, consectetur adipiscing Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce accumsan risus et hendrerit finibus. Quisque eleifend sagittis mauris eget iaculis.
						</p>
					
						<div class="gridle-row">
							<div class="gridle-gr-9">
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="http://placehold.it/656x369">
									</a>
								</figure>
							</div>
						</div>
						<p class="common-box__excerpt">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce accumsan risus et hendrerit finibus. Quisque eleifend sagittis mauris eget iaculis. In tempus nunc arcu, sed semper metus facilisis sit amet. Suspendisse placerat mi purus, vel ornare sem varius et. Quisque ullamcorp.
						</p>
						
						<article class="horizon">
							<h2 class="title-tiny">
								Listado
							</h2>
							<p class="common-box__excerpt">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce accumsan risus et hendrerit finibus. Quisque eleifend sagittis mauris eget iaculis. In tempus nunc arcu, sed semper metus facilisis sit amet. Suspendisse placerat mi purus, vel ornare sem varius et. Quisque ullamcorp.
							</p>

							<ol>
						   		<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
						   		<li>Aliquam tincidunt mauris eu risus.</li>
						   		<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
						   		<li>Aliquam tincidunt mauris eu risus.</li>
							</ol>

							<p class="common-box__excerpt">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce accumsan risus et hendrerit finibus. Quisque eleifend sagittis mauris eget iaculis. In tempus nunc arcu, sed semper metus facilisis sit amet. Suspendisse placerat mi purus, vel ornare sem varius et. Quisque ullamcorper dictum odio vitae malesuada. Proin venenatis consequat fermentum. Donec tristique euismod nunc, nec pellentesque nisl efficitur ac. Sed quis tincidunt turpis. Proin tempor sollicitudin lorem. Donec rutrum in purus quis placerat. Vivamus scelerisque blandit sollicitudin. Nam nec risus non justo molestie ultrices. Proin quis vestibulum magna. Fusce nec felis laoreet, egestas odio sit amet, dapibus dolor.
							</p>
							
							<p class="common-box__excerpt">
								Nulla tristique magna sem, eu malesuada nisi dictum sed. Curabitur nibh metus, congue tincidunt arcu in, vestibulum hendrerit dui. Ut eu dolor risus. Maecenas sodales, velit quis ornare pharetra, mi urna cursus lacus, sit amet faucibus tortor ex sed nisl. Fusce sed lectus elementum, sollicitudin massa ut, faucibus sem. Mauris ut dolor et ante fringilla euismod. Nullam bibendum purus ut varius gravida. Nullam rutrum sodales lectus, ut fermentum nunc vehicula ac. Mauris porttitor sodales risus, sit amet cursus nisi. Sed efficitur tortor dui. Aenean in dui laoreet, rhoncus elit eget, dapibus sapien. Maecenas ornare eros eget ligula ultricies, nec dictum lectus vestibulum. Mauris ullamcorper lorem nec gravida vehicula. Sed sem elit, viverra lobortis condimentum et, ullamcorper eu dui.
							</p>
							
							<p class="common-box__excerpt">
								Sed consectetur justo et tellus imperdiet, rutrum luctus eros pellentesque. Integer lobortis ex sed accumsan venenatis. Morbi a lectus sit amet enim interdum eleifend ut ut nisl. Integer iaculis, elit quis bibendum dapibus, velit nisi condimentum ipsum, at auctor dolor enim ut magna. In vel tristique nisi. Nulla facilisi. Phasellus elementum purus vel interdum blandit.
							</p>
						</article>
					</div>
						
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<?php include('sidebar.php'); ?>
					</div>
				</div>
			</div>
		</section>
	</main>
<?php include('footer.php'); ?>
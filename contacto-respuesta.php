<?php include('header.php'); ?>

<main>
	<section class="horizon__search bg-white">
		<div class="container gridle-no-gutter">
			<div class="gridle-row">
				<div class="gridle-gr-9 gridle-gr-12@medium">
					<?php include('partials/breadcrumbs.php'); ?>
				</div>

				<div class="gridle-gr-3 gridle-gr-12@medium">
					<?php include('partials/searchbar.php'); ?>
				</div>
			</div>
		</div>
	</section>

	<section class="bg-white">
		<div class="container gridle-no-gutter">
			<div class="gridle-row">
				<div class="gridle-gr-12 gridle-gr-12@medium">
					<h1 class="title">
						Contacto
					</h1>
					<p class="common-box__featured">
						Texto de la caja, como máximo tiene una extensión de 140 caracteres Lorem ipsum dolor sit amet, consectetur adipiscing
					</p>
				</div>

				<div class="gridle-gr-6 gridle-gr-12@medium">
					<div class="common-box__body">
						<h2 class="main-title--sub">
							¡Gracias por contactarnos!
						</h2>

						<p class="common-box__excerpt">
							Te responderemos a la brevedad.
						</p>

						<p class="common-box__excerpt">
							¡Recuerda! Si deseas saber cómo realizar una compra ingresa a
						</p>

						<p class="common-box__plus">
							<a href="#" title="titulo" class="font-color-grey-darkest text-size-tiny">¿Cómo comprar?</a>
						</p>
					</div>
				</div>

				<div class="gridle-gr-6 gridle-gr-12@medium">
					<div class="gridle-gr-12 gridle-gr-12@medium">
						<?php include('partials/googlemap.php'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<?php include('footer.php'); ?>
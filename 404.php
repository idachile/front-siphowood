<?php include('header.php'); ?>
<main>
	<section class="horizon__search bg-white">
		<div class="container gridle-no-gutter">
			<div class="gridle-row">
				<div class="gridle-gr-9 gridle-gr-12@medium">
					<?php include('partials/breadcrumbs.php'); ?>
				</div>

				<div class="gridle-gr-3 gridle-gr-12@medium">
					<?php include('partials/searchbar.php'); ?>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon bg-white">
		<div class="container gridle-no-gutter">
			<div class="gridle-row">
				<div class="gridle-gr-12 gridle-gr-12@medium">
					<p class="common-box__meta font-size-regular">ERROR 404</p>
					<h4 class="title">
						Página no encontrada
					</h4>
					<p class="common-box__excerpt">
						Lo sentimos, la página que buscas fue modificada, borrada o no existe.
					</p>

					<p class="common-box__excerpt">
						Te invitamos a continuar tu navegación volviendo a la página de inicio, o visitar <strong>categorias destacadas:</strong>
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon horizon__inner bg-lines">
		<div class="container gridle-no-gutter">
			<div class="gridle-row">
				<div class="gridle-gr-6 gridle-gr-12@medium">
					<h2 class="main-title">
						<a href="#" title="titulo">Categoría destacada 1</a>
					</h2>
					<div class="gridle-row">
						<div class="gridle-gr-6 gridle-gr-12@medium">
							<article>
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-4.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Tadao Azul</a>
									</h2>

									<p class="common-box__excerpt">
										Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
									</p>

									<p class="common-box__extra">
										<span class="common-box__tag">$42.900</span>
										<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right">
											<span>Ver detalles</span>
											<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
										</a>
									</p>
								</div>
							</article>
						</div>
						<div class="gridle-gr-6 gridle-gr-12@medium">
							<article>
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-7.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Yokito Calabaza</a>
									</h2>

									<p class="common-box__excerpt">
										Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
									</p>

									<p class="common-box__extra">
										<span class="common-box__tag">$5.000</span>
										<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right">
											<span>Ver detalles</span>
											<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
										</a>
									</p>
								</div>
							</article>
						</div>
					</div>
				</div>
				<div class="gridle-gr-6 gridle-gr-12@medium">
					<h2 class="main-title">
						<a href="#" title="titulo">Categoría destacada 2</a>
					</h2>
					<div class="gridle-row">
						<div class="gridle-gr-6 gridle-gr-12@medium">
							<article>
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-8.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Rucksack gris</a>
									</h2>

									<p class="common-box__excerpt">
										Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
									</p>

									<p class="common-box__extra">
										<span class="common-box__tag">$5.000</span>
										<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right">
											<span>Ver detalles</span>
											<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
										</a>
									</p>
								</div>
							</article>
						</div>
						<div class="gridle-gr-6 gridle-gr-12@medium">
							<article>
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-9.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Tadao Verde Bosque</a>
									</h2>

									<p class="common-box__excerpt">
										Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
									</p>

									<p class="common-box__extra">
										<span class="common-box__tag">$5.000</span>
										<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right">
											<span>Ver detalles</span>
											<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
										</a>
									</p>
								</div>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>

<?php include('footer.php'); ?>
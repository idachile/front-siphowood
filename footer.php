<footer class="footer-bar">
	<div class="container gridle-no-gutter">
		<div class="gridle-row">
			<div class="gridle-gr-3 gridle-gr-12@medium">
				<a class="app-brand" href="index.html">
					<img src="http://placehold.it/80x45" alt="Logo" class="app-brand__logo">
					<span class="app-brand__name">Siphowood</span>
				</a>
			</div>
			<div class="gridle-gr-2 gridle-gr-12@medium">
				<a href="#" title="titulo" class="footer-bar__link">Baobab</a>
				<a href="#" title="titulo" class="footer-bar__link">Quiver</a>
				<a href="#" title="titulo" class="footer-bar__link">Sycamore</a>
			</div>
			<div class="gridle-gr-2 gridle-gr-12@medium">
				<a href="#" title="titulo" class="footer-bar__link">Mopane</a>
				<a href="#" title="titulo" class="footer-bar__link">Marula</a>
			</div>
			<div class="gridle-gr-2 gridle-gr-12@medium">
				<a href="#" title="titulo" class="footer-bar__link">Preguntas Frecuentes</a>
			</div>
			<div class="gridle-gr-3 gridle-gr-12@medium">
				<p>Pedro de Valdivia #1439,</p>
				<p>Providencia, Santiago</p>
				<p>+56 9 235627</p>
				<p><a href="#" title="titulo">contacto@empresa.cl</a></p>

				<p>
					<a href="#" title="titulo" class="social-link social-link--facebook">Facebook</a>
					<a href="#" title="titulo" class="social-link social-link--twitter">Twitter</a>
					<a href="#" title="titulo" class="social-link social-link--instagram">Instagram</a>
				</p>
			</div>
		</div>
	</div>
</footer>
<script src="scripts/min/main.min.js"></script>
</body>

</html>
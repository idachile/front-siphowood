<?php include('header.php'); ?>

	<main>
		<section class="horizon__search bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-9 gridle-gr-12@medium">
						<?php include('partials/breadcrumbs.php'); ?>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<?php include('partials/searchbar.php'); ?>
					</div>
				</div>
			</div>
		</section>

		<section class="horizon bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-10 gridle-gr-12@medium">
						<h2 class="title">
							Baobab
						</h2>
					</div>
					<div class="gridle-gr-2 gridle-gr-12@medium">
						<p class="sub-menu__holder font-righted">
							<i class="icon-elem icon-elem--chevron_left" ></i>
							<a href="index.html" class="font-color-grey-dark sub-menu__link">Volver a las categorías</a> 
						</p>
					</div>
				</div>	

				<div class="gridle-row">
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-4.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Tadao Azul</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$42.900</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-7.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Yokito Calabazz</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-8.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Rucksack gris</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-9.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Tadao Verde Bosque</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
				</div>

				<div class="gridle-row">
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-main-3.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Tadao Azul</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$42.900</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-main-4.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Yokito Calabazz</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-main-3.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Rucksack gris</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
				
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-main-4.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Tadao Verde Bosque</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
				</div>

				<div class="gridle-row">
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-main-4.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Tadao Azul</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$42.900</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-main-3.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Yokito Calabazz</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-main-4.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Rucksack gris</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-main-3.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Tadao Verde Bosque</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
				</div>

				<div class="gridle-row">
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-main-3.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Tadao Azul</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$42.900</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
		
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-main-4.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Yokito Calabazz</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-main-3.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Rucksack gris</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
			
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-main-4.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Tadao Verde Bosque</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
				</div>
			</div>
			<div class="font-centered">
				<a href="#" title="titulo" class="button button--ghost button--fixed button--large" >Ver más productos</a>
			</div>
		</section>

		<section class="horizon horizon__inner bg-lines">
			<div class="container gridle-no-gutter ">
				<div class="gridle-row">
					<?php include('partials/categorias-destacadas.php'); ?>
				</div>
			</div>
		</section>
	</main>

<?php include('footer.php'); ?>
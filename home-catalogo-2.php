<?php include('header.php');?>

	<main>
		<section class="horizon__search bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-10 gridle-gr-12@medium gridle-gr-centered">
						<?php include('partials/searchbar.php');?>
					</div>
				</div>
			</div>
		</section>

		<section class="horizon bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-7 gridle-gr-12@medium gridle-no-gutter-top gridle-no-gutter-bottom">
						<article class="common-box--fancy">
							<figure class="common-box__figure">
								<img src="http://placehold.it/737x570">
							</figure>
							<div class="common-box__body">
								<h2 class="main-title">
									<a href="#" title="titulo" >Dumbo Negro edición especial primavera-verano 2017</a>
								</h2>

								<div class="common-box__footer">
									<span class="common-box__tag common-box__tag--highlighted">$4.990</span>

									<a href="#" title="titulo" class="button button--more button--small button--ghost float-right" >Ver detalles</a>
								</div>
							</div>
						</article>
					</div>
					<div class="gridle-gr-5 gridle-gr-12@medium gridle-no-gutter-top gridle-no-gutter-bottom">
						<article class="common-box--fancy common-box--fancy__left">
							<figure class="common-box__figure">
								<img src="http://placehold.it/525x284">
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Tadao Berenjena</a>
								</h2>

								<div class="common-box__footer">
									<span class="common-box__tag common-box__tag--faded common-box__tag--extra">$30.990</span>
									<span class="common-box__tag common-box__tag--highlighted">$19.990</span>

									<a href="#" title="titulo" class="button button--more button--black__right button--small button--ghost float-right" >Ver detalles</a>
								</div>
							</div>
						</article>
						<article class="common-box--fancy">
							<figure class="common-box__figure">
								<img src="http://placehold.it/525x284">
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Dumbo Negro</a>
								</h2>

								<div class="common-box__footer">
									<span class="common-box__tag common-box__tag--faded common-box__tag--extra">$30.990</span>
									<span class="common-box__tag common-box__tag--highlighted">$19.990</span>

									<a href="#" title="titulo" class="button button--more button--black__right button--small button--ghost float-right" >Ver detalles</a>
								</div>
							</div>
						</article>
					</div>
				</div>
			</div>
		</section>

		<section class="horizon horizon__inner bg-lines">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-6 gridle-gr-12@medium">
						<h2 class="main-title">
							<a href="#" title="titulo">Baobab</a>
						</h2>
						<div class="gridle-row">
							<div class="gridle-gr-6 gridle-gr-12@medium">
								<article>
									<figure class="common-box__figure">
										<a href="#" title="titulo">
											<img src="images/img/img-product-4.jpg">
										</a>
									</figure>
									<div class="common-box__body">
										<h2 class="main-title--tiny">
											<a href="#" title="titulo" >Tadao Azul</a>
										</h2>

										<p class="common-box__excerpt">
											Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
										</p>

										<p class="common-box__extra">
											<span class="common-box__tag">$42.900</span>
											<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
												<span>Ver detalles</span>
												<span class="icon-elem icon-elem--chevron_right font-color-black" ></span>
											</a>
										</p>
									</div>
								</article>
							</div>
							<div class="gridle-gr-6 gridle-gr-12@medium">
								<article>
									<figure class="common-box__figure">
										<a href="#" title="titulo">
											<img src="images/img/img-product-7.jpg">
										</a>
									</figure>
									<div class="common-box__body">
										<h2 class="main-title--tiny">
											<a href="#" title="titulo" >Yokito Calabazz</a>
										</h2>

										<p class="common-box__excerpt">
											Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
										</p>

										<p class="common-box__extra">
											<span class="common-box__tag">$5.000</span>

											<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
												<span>Ver detalles</span>
												<span class="icon-elem icon-elem--chevron_right font-color-black" ></span>
											</a>
										</p>
									</div>
								</article>
							</div>
						</div>
					</div>
					<div class="gridle-gr-6 gridle-gr-12@medium">
						<h2 class="main-title">
							<a href="#" title="titulo">Quiver</a>
						</h2>
						<div class="gridle-row">
							<div class="gridle-gr-6 gridle-gr-12@medium">
								<article>
									<figure class="common-box__figure">
										<a href="#" title="titulo">
											<img src="images/img/img-product-8.jpg">
										</a>
									</figure>
									<div class="common-box__body">
										<h2 class="main-title--tiny">
											<a href="#" title="titulo" >Rucksack gris</a>
										</h2>

										<p class="common-box__excerpt">
											Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
										</p>

										<p class="common-box__extra">
											<span class="common-box__tag">$5.000</span>
											<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
												<span>Ver detalles</span>
												<span class="icon-elem icon-elem--chevron_right font-color-black" ></span>
											</a>
										</p>
									</div>
								</article>
							</div>
							<div class="gridle-gr-6 gridle-gr-12@medium">
								<article>
									<figure class="common-box__figure">
										<a href="#" title="titulo">
											<img src="images/img/img-product-9.jpg">
										</a>
									</figure>
									<div class="common-box__body">
										<h2 class="main-title--tiny">
											<a href="#" title="titulo" >Tadao Verde Bosque</a>
										</h2>

										<p class="common-box__excerpt">
											Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
										</p>

										<p class="common-box__extra">
											<span class="common-box__tag">$5.000</span>
											<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
												<span>Ver detalles</span>
												<span class="icon-elem icon-elem--chevron_right font-color-black" ></span>
											</a>
										</p>
									</div>
								</article>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="horizon horizon__inner bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-12 gridle-gr-12@medium">
						<h2 class="main-title">
							<a href="#" title="titulo">Ubicación</a>
						</h2>
						<div class="gridle-row">
							<div class="gridle-gr-12 gridle-gr-12@medium">
								<?php include('partials/googlemap.php');?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>

	<?php include('footer.php');?>
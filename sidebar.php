<article class="content-box--grey content__sidebar">
    <div class="common-box__body">
        <p class="common-box__excerpt font-size-regular">Te invitamos a navegar dentro de <strong>nuestras categorías</strong></p>
        <ul>
            <li><a href="#" title="titulo" class="font-color-grey-darkest font-size-regular">Categoría 1</a></li>
			<li><a href="#" title="titulo" class="font-color-grey-darkest font-size-regular">Categoría 2</a></li>
			<li><a href="#" title="titulo" class="font-color-grey-darkest font-size-regular">Categoría 3</a></li>
			<li><a href="#" title="titulo" class="font-color-grey-darkest font-size-regular">Categoría 4</a></li>
			<li><a href="#" title="titulo" class="font-color-grey-darkest font-size-regular">Categoría 5</a></li>
		</ul>
	</div>
</article>
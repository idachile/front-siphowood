<?php include('header.php'); ?>
<main>
	<section class="horizon__search bg-white">
		<div class="container gridle-no-gutter">
			<div class="gridle-row">
				<div class="gridle-gr-10 gridle-gr-12@medium gridle-gr-centered">
					<?php include('partials/searchbar.php'); ?>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon bg-white">
		<div class="container gridle-no-gutter ">
			<div class="gridle-row">
				<div class="gridle-gr-12 gridle-gr-12@medium">
					<article class="common-box--horizontal common-box--featured">
						<figure class="common-box__figure">
							<div class="common-box__header">
								<div class="overlap">Destacado</div>
							</div>
							<a href="#" title="titulo">
								<img src="http://placehold.it/752x423">
							</a>
						</figure>
						<div class="common-box__body">
							<h2 class="main-title">
								<a href="#" title="titulo">Producto XY</a>
							</h2>

							<p class="common-box__meta"><em>Marca</em></p>

							<p class="common-box__excerpt">
								Texto de la caja, como máximo tiene una extensión de 140 caracteres Lorem ipsum dolor sit amet, consectetur adipiscing elit.
								Maecenas ele ...
							</p>

							<p class="common-box__extra">
								<span class="common-box__tag">$19.990</span>
							</p>

							<div class="common-box__extra">
								<a href="#" title="titulo" class="button button--ghost button--more button--more__main">Detalles del producto</a>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon horizon__inner bg-lines">
		<div class="container gridle-no-gutter">
			<div class="gridle-row">
				<div class="gridle-gr-6 gridle-gr-12@medium">
					<h2 class="main-title">
						<a href="#" title="titulo">Baobab</a>
					</h2>
					<div class="gridle-gr-12 gridle-gr-12@medium">
						<div class="gridle-row">

							<div class="gridle-gr-12 gridle-gr-12@medium">
								<article class="common-box--horizontal">
									<figure class="common-box__figure">
										<a href="#" title="titulo">
											<img src="images/img/img-product-4.jpg">
										</a>
									</figure>
									<div class="common-box__body">
										<h2 class="main-title--tiny">
											<a href="#" title="titulo">Producto XY</a>
										</h2>

										<p class="common-box__excerpt">
											Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
										</p>

										<p class="font-size-tiny">
											<strong>STOCK: 100 productos</strong>
										</p>

										<p class="common-box__extra">
											<span class="common-box__tag">$5.000</span>
											<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right">
												<span>Ver detalles</span>
												<span class="icon-elem icon-elem--chevron_right font-color-black"></span>
											</a>
										</p>
									</div>
								</article>
							</div>
						</div>
					</div>
				</div>
				<div class="gridle-gr-6 gridle-gr-12@medium">
					<h2 class="main-title">
						<a href="#" title="titulo">Quiver</a>
					</h2>
					<div class="gridle-gr-12 gridle-gr-12@medium">
						<div class="gridle-row">
							<div class="gridle-gr-12 gridle-gr-12@medium">
								<article class="common-box--horizontal">
									<figure class="common-box__figure">
										<a href="#" title="titulo">
											<img src="images/img/img-product-8.jpg">
										</a>
									</figure>
									<div class="common-box__body">
										<h2 class="main-title--tiny">
											<a href="#" title="titulo">Producto XY</a>
										</h2>

										<p class="common-box__excerpt">
											Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
										</p>

										<p class="font-size-tiny">
											<strong>STOCK: 100 productos</strong>
										</p>

										<p class="common-box__extra">
											<span class="common-box__tag">$5.000</span>
											<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right">
												<span>Ver detalles</span>
												<span class="icon-elem icon-elem--chevron_right font-color-black"></span>
											</a>
										</p>
									</div>
								</article>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon horizon__inner bg-white">
		<div class="container gridle-no-gutter">
			<div class="gridle-row">
				<div class="gridle-gr-6 gridle-gr-12@medium">
					<h2 class="main-title">
						<a href="#" title="titulo">Últimas Novedades</a>
					</h2>
					<div class="gridle-row">
						<?php include('partials/novedades-horizontal.php'); ?>
					</div>
				</div>
				<div class="gridle-gr-6 gridle-gr-12@medium">
					<h2 class="main-title">
						<a href="#" title="titulo">Ubicación</a>
					</h2>
					<div class="gridle-row">
						<div class="gridle-gr-12 gridle-gr-12@medium">
							<?php include('partials/googlemap.php'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<?php include('footer.php'); ?>
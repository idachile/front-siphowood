<?php include('header.php'); ?>
	<main>
		<section class="horizon__search bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-9 gridle-gr-12@medium">
						<?php include('partials/breadcrumbs.php'); ?>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<?php include('partials/searchbar.php'); ?>
					</div>
				</div>
			</div>
		</section>

		<section class="horizon horizon__inner bg-white horizon">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-12 gridle-gr-12@medium">
						<h2 class="title">
							Resultados de búsqueda
						</h2>
						<p class="common-box__excerpt">
							Se encontraron <strong><em>7 resultados </em></strong>para la búsqueda <strong><em>"looks"</em></strong>
						</p>
					</div>
				</div>
				<div class="gridle-row">
					<div class="gridle-gr-9 gridle-gr-12@medium">
						<div class="gridle-row">
							<div class="gridle-gr-4 gridle-gr-12@medium">
								<article>
									<figure class="common-box__figure">
										<a href="#" title="titulo">
											<img src="images/img/img-product-4.jpg">
										</a>
									</figure>
									<div class="common-box__body">
										<h2 class="main-title--tiny">
											<a href="#" title="titulo" >Tadao Azul</a>
										</h2>

										<p class="common-box__excerpt">
											Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
										</p>

										<p class="common-box__extra">
											<span class="common-box__tag">$42.900</span>
											<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
												<span>Ver detalles</span>
												<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
											</a>
										</p>
									</div>
								</article>
							</div>
							<div class="gridle-gr-4 gridle-gr-12@medium">
								<article>
									<figure class="common-box__figure">
										<a href="#" title="titulo">
											<img src="images/img/img-product-7.jpg">
										</a>
									</figure>
									<div class="common-box__body">
										<h2 class="main-title--tiny">
											<a href="#" title="titulo" >Yokito Calabaza</a>
										</h2>

										<p class="common-box__excerpt">
											Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
										</p>

										<p class="common-box__extra">
											<span class="common-box__tag">$5.000</span>
											<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
												<span>Ver detalles</span>
												<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
											</a>
										</p>
									</div>
								</article>
							</div>
							<div class="gridle-gr-4 gridle-gr-12@medium">
								<article>
									<figure class="common-box__figure">
										<a href="#" title="titulo">
											<img src="images/img/img-product-8.jpg">
										</a>
									</figure>
									<div class="common-box__body">
										<h2 class="main-title--tiny">
											<a href="#" title="titulo" >Rucksack Gris</a>
										</h2>

										<p class="common-box__excerpt">
											Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
										</p>

										<p class="common-box__extra">
											<span class="common-box__tag">$5.000</span>
											<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
												<span>Ver detalles</span>
												<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
											</a>
										</p>
									</div>
								</article>
							</div>
						</div>

						<div class="gridle-row">
							<div class="gridle-gr-4 gridle-gr-12@medium">
								<article>
									<figure class="common-box__figure">
										<a href="#" title="titulo">
											<img src="images/img/img-product-9.jpg">
										</a>
									</figure>
									<div class="common-box__body">
										<h2 class="main-title--tiny">
											<a href="#" title="titulo" >Tadao Verde Bosque</a>
										</h2>

										<p class="common-box__excerpt">
											Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
										</p>

										<p class="common-box__extra">
											<span class="common-box__tag">$5.000</span>
											<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
												<span>Ver detalles</span>
												<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
											</a>
										</p>
									</div>
								</article>
							</div>
							<div class="gridle-gr-4 gridle-gr-12@medium">
								<article>
									<figure class="common-box__figure">
										<a href="#" title="titulo">
											<img src="images/img/img-product-9.jpg">
										</a>
									</figure>
									<div class="common-box__body">
										<h2 class="main-title--tiny">
											<a href="#" title="titulo" >Tadao Verde Bosque</a>
										</h2>

										<p class="common-box__excerpt">
											Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
										</p>

										<p class="common-box__extra">
											<span class="common-box__tag">$5.000</span>
											<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
												<span>Ver detalles</span>
												<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
											</a>
										</p>
									</div>
								</article>
							</div>
							<div class="gridle-gr-4 gridle-gr-12@medium">
								<article>
									<figure class="common-box__figure">
										<a href="#" title="titulo">
											<img src="images/img/img-product-7.jpg">
										</a>
									</figure>
									<div class="common-box__body">
										<h2 class="main-title--tiny">
											<a href="#" title="titulo" >Yokito Calabaza</a>
										</h2>

										<p class="common-box__excerpt">
											Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
										</p>

										<p class="common-box__extra">
											<span class="common-box__tag">$5.000</span>
											<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
												<span>Ver detalles</span>
												<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
											</a>
										</p>
									</div>
								</article>
							</div>
						</div>

						<div class="gridle-row">
							<div class="gridle-gr-4 gridle-gr-12@medium">
								<article>
									<figure class="common-box__figure">
										<a href="#" title="titulo">
											<img src="images/img/img-product-8.jpg">
										</a>
									</figure>
									<div class="common-box__body">
										<h2 class="main-title--tiny">
											<a href="#" title="titulo" >Rucksack Gris</a>
										</h2>

										<p class="common-box__excerpt">
											Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
										</p>

										<p class="common-box__extra">
											<span class="common-box__tag">$5.000</span>
											<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
												<span>Ver detalles</span>
												<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
											</a>
										</p>
									</div>
								</article>
							</div>
						</div>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<?php include('sidebar.php'); ?>
					</div>
				</div>
			</div>
		</section>
	</main>

<?php include('footer.php'); ?>
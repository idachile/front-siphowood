<?php include('header.php'); ?>

	<main>
		<section class="horizon__search bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-9 gridle-gr-12@medium">
						<?php include('partials/breadcrumbs.php'); ?>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<?php include('partials/searchbar.php'); ?>
					</div>
				</div>
			</div>
		</section>

		<section class="horizon bg-lines">
			<div class="container gridle-no-gutter ">
				<div class="gridle-row">
					<div class="gridle-gr-7 gridle-gr-12@medium">
						<h2 class="title">
							Baobab
						</h2>
					</div>
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<p class="common-box__meta float-right">Encuentra lo que buscas en <strong>Baobab</strong></p>
					</div>
					<div class="gridle-gr-2 gridle-gr-12@medium">
						<div class="form-control" data-error-message="Debe seleccionar una opción">
							<select class="form-control__field form-control__field--select--options" required >
								<option value="" class="font-color-grey-dark">Subcategorías</option>
								<option value="1">Subcategoría 1</option>
								<option value="2">Subcategoría 2</option>
								<option value="3">Subcategoría 3</option>
								<option value="4">Subcategoría 4</option>
								<option value="5">Subcategoría 5</option>
							</select>
						</div>       
					</div>
				</div>	
				<div class="gridle-row">
					<div class="gridle-gr-12 gridle-gr-12@medium">
						<article class="common-box--horizontal common-box--featured">
							<figure class="common-box__figure">
								<div class="common-box__header">
									<div class="overlap" >Destacado</div>
								</div>
								<a href="#" title="titulo">
									<img src="http://placehold.it/752x423">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title">
									<a href="#" title="titulo" >Producto XY</a>
								</h2>
										
								<p class="common-box__meta"><em>Nombre de la marca</em></p>

								<p class="common-box__excerpt">
									Texto de la caja, como máximo tiene una extensión de 140 caracteres Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ele ...
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$19.990</span>
								</p>

								<div class="common-box__extra">
									<a href="#" title="titulo" class="button button--ghost button--more button--more__main" >Detalles del producto</a>
								</div>
							</div>
						</article>
					</div>
				</div>
			</div>
		</section>

		<section class="horizon horizon__inner bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-4.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Tadao Azul</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$42.900</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-7.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Yokito Calabazz</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-8.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Rucksack gris</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-9.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Tadao Verde Bosque</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
				</div>
				
				<div class="gridle-row">
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-9.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Tadao Azul</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$42.900</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
					
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-2.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Yokito Calabazz</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
			
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-8.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Rucksack gris</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
					
					<div class="gridle-gr-3 gridle-gr-12@medium">
						<article>
							<figure class="common-box__figure">
								<a href="#" title="titulo">
									<img src="images/img/img-product-3.jpg">
								</a>
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Tadao Verde Bosque</a>
								</h2>

								<p class="common-box__excerpt">
									Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
								</p>

								<p class="common-box__extra">
									<span class="common-box__tag">$5.000</span>
									<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
										<span>Ver detalles</span>
										<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
									</a>
								</p>
							</div>
						</article>
					</div>
				</div>
			</div>
			<div class="font-centered">
				<a href="#" title="titulo" class="button button--ghost button--fixed button--large" >Ver más productos</a>
			</div>
		</section>

		<section class="horizon horizon__inner bg-lines">
			<div class="container gridle-no-gutter ">
				<div class="gridle-row">
					<?php include('partials/categorias-destacadas.php'); ?>
				</div>
			</div>
		</section>
	</main>

<?php include('footer.php'); ?>
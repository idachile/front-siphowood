<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title>Siphowood</title>
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
</head>
<body>
	<header class="header-bar" >
		<section class="top-bar">
			<div class="container" data-area-name="menu-top" >
				<div class="top-bar__items" data-mq-change="medium" data-match-area="menu-top@medium" data-unmatch-area="menu-top" >
					<a href="como-comprar.php" class="top-bar__item top-bar__item--current">¿Cómo comprar?</a>
					<a href="quienes-somos.php" class="top-bar__item">¿Quiénes somos?</a>
					<a href="ultimas-novedades.php" class="top-bar__item">Últimas novedades</a>
					<a href="contacto.php" class="top-bar__item">Contacto</a>
					<a href="404.php" title="titulo" class="top-bar__item">404</a>
					<a href="#" title="titulo" class="hide-on-medium-down social-link social-link--facebook">Facebook</a>
					<a href="#" title="titulo" class="hide-on-medium-down social-link social-link--twitter">Twitter</a>
					<a href="#" title="titulo" class="hide-on-medium-down social-link social-link--instagram">Instagram</a>
				</div>
			</div>
		</section>

		<nav class="nav-bar" data-module="nav-bar" >
			<div class="container">
				<div class="nav-bar__holder">
					<div class="nav-bar__brand">
						<a class="app-brand app-brand--inline" href="index.php">
							<img class="app-brand__logo" src="http://placehold.it/80x45" alt="Logo" >
							<!--<span class="app-brand__name">Siphowood</span>-->
						</a>
					</div>
					<div class="nav-bar__body" data-role="nav-body">
						<button class="nav-bar__deploy-button" aria-label="Ver menú" data-role="nav-deployer" >
							<span></span>
						</button>
						<div class="nav-bar__menu-holder" >
							<ul class="nav-bar__menu">
								<li class="menu-item menu-item--current">
									<a class="menu-item__link" href="catalogo-2.php" title="titulo">Catálogo</a>
								</li>
								<li class="menu-item" >
									<a class="menu-item__link" href="home-catalogo-1.php" title="titulo">Home Catálogo 1</a>
								</li>
								<li class="menu-item" >
									<a class="menu-item__link" href="home-catalogo-2.php" title="titulo">Home Catálogo 2</a>
								</li>
								<li class="menu-item" >
									<a class="menu-item__link" href="portadilla-subcategoria.php" title="titulo">Subcategoría</a>
								</li>
								<li class="menu-item menu-item--has-submenu" data-role="touch-submenu" title="titulo">
									<a class="menu-item__link" href="#" data-role="touch-submenu-deployer">Producto</a>
									<ul class="submenu">
										<li class="submenu__item submenu__item--current"><a href="producto-full.php" title="titulo">Producto Full</a></li>
										<li class="submenu__item"><a href="producto-catgen.php" title="titulo">Producto Cat. General</a></li>
										<li class="submenu__item"><a href="producto-ficha.php" title="titulo">Producto Ficha</a></li>
										<li class="submenu__item"><a href="producto-galeria.php" title="titulo">Producto Galeria</a></li>
										<li class="submenu__item"><a href="producto-media.php" title="titulo">Producto Media</a></li>
									</ul>
								</li>
							</ul>

							<div class="nav-bar__auxmenu" data-area-name="menu-top@medium"></div>
						</div>
					</div>
				</div>
			</div>
		</nav>
	</header>
<?php include('header.php');?>

<main>
	<section class="horizon__search">
		<div class="container gridle-no-gutter">
			<div class="gridle-row">
				<div class="gridle-gr-10 gridle-gr-12@medium gridle-gr-centered">
					<?php include('partials/searchbar.php');?>
				</div>
			</div>

			<div class="gridle-row">
				<div class="gridle-gr-12 gridle-gr-12@medium <gridle-no-gutter></gridle-no-gutter>">
					<article class="common-box--fancy">
						<figure class="common-box__figure">
							<img src="http://placehold.it/1200x450">
						</figure>
						<div class="common-box--body__medium">
							<p class="main-title main-title_center">
								<a href="#" title="titulo">Dumbo Negro edición especial primavera-verano 2017</a>
							</p>

							<a href="#" title="titulo" class="button button--black__box button--small button--ghost">Ver detalles</a>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon horizon__inner bg-lines">
		<div class="container gridle-no-gutter">
			<div class="gridle-row">
				<div class="gridle-gr-6 gridle-gr-12@medium">
					<h2 class="main-title">
						<a href="#" title="titulo">Baobab</a>
					</h2>
					<div class="gridle-row">
						<div class="gridle-gr-6 gridle-gr-12@medium">
							<article>
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-4.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Tadao Azul</a>
									</h2>

									<p class="common-box__excerpt">
										Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
									</p>

									<p class="common-box__extra">
										<span class="common-box__tag">$42.900</span>

										<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right">
											<span>Ver detalles</span>
											<span class="icon-elem icon-elem--chevron_right font-color-black"></span>
										</a>
									</p>
								</div>
							</article>
						</div>
						<div class="gridle-gr-6 gridle-gr-12@medium">
							<article>
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-7.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Yokito Calabazz</a>
									</h2>

									<p class="common-box__excerpt">
										Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
									</p>

									<p class="common-box__extra">
										<span class="common-box__tag">$5.000</span>

										<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right">
											<span>Ver detalles</span>
											<span class="icon-elem icon-elem--chevron_right font-color-black"></span>
										</a>
									</p>
								</div>
							</article>
						</div>
					</div>
				</div>
				<div class="gridle-gr-6 gridle-gr-12@medium">
					<h2 class="main-title">
						<a href="#" title="titulo">Quiver</a>
					</h2>

					<div class="gridle-row">
						<div class="gridle-gr-6 gridle-gr-12@medium">
							<article>
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-8.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Rucksack gris</a>
									</h2>

									<p class="common-box__excerpt">
										Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
									</p>

									<p class="common-box__extra">
										<span class="common-box__tag">$5.000</span>
										<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right">
											<span>Ver detalles</span>
											<span class="icon-elem icon-elem--chevron_right font-color-black"></span>
										</a>
									</p>
								</div>
							</article>
						</div>
						<div class="gridle-gr-6 gridle-gr-12@medium">
							<article>
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-9.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Tadao Verde Bosque</a>
									</h2>

									<p class="common-box__excerpt">
										Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
									</p>

									<p class="common-box__extra">
										<span class="common-box__tag">$5.000</span>
										<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right">
											<span>Ver detalles</span>
											<span class="icon-elem icon-elem--chevron_right font-color-black"></span>
										</a>
								</div>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon horizon__inner bg-white">
		<div class="container gridle-no-gutter">
			<div class="gridle-row">
				<div class="gridle-gr-12 gridle-gr-12@medium">
					<h2 class="main-title">
						<a href="#" title="titulo">Últimas Novedades</a>
					</h2>
					<div class="gridle-row">
						<div class="gridle-gr-6 gridle-gr-12@medium">
							<article class="common-box--horizontal">
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-4.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<p class="common-box__meta">Epígrafe de la noticia o artículo</p>

									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Título en una línea</a>
									</h2>

									<p class="common-box__excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus feugiat porta efficitur. In lacinia ac arcu eu
										viverra.</p>
								</div>
							</article>
						</div>
						<div class="gridle-gr-6 gridle-gr-12@medium">
							<article class="common-box--horizontal">
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-7.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<p class="font-size-tiny">
										STOCK: 100 productos
									</p>

									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Título en una línea</a>
									</h2>

									<p class="common-box__excerpt">Nuevo modelo de nuestro producto XY, llega a Chile el 2 de Abril, con importantes incorporaciones que hacen que
										este nuevo modelo.</p>
								</div>
							</article>
						</div>

						<div class="gridle-gr-6 gridle-gr-12@medium">
							<article class="common-box--horizontal">
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-8.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<p class="font-size-tiny">
										STOCK: 100 productos
									</p>

									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Título de la Noticias en dos líneas de extención</a>
									</h2>

									<p class="common-box__excerpt">Nuevo modelo de nuestro producto XY, llega a Chile el 2 de Abril, con importantes incorporaciones que hacen que
										este nuevo modelo.</p>
								</div>
							</article>
						</div>
						<div class="gridle-gr-6 gridle-gr-12@medium">
							<article class="common-box--horizontal">
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-9.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<p class="font-size-tiny">
										STOCK: 100 productos
									</p>

									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Título en una línea</a>
									</h2>

									<p class="common-box__excerpt">Nuevo modelo de nuestro producto XY, llega a Chile el 2 de Abril, con importantes incorporaciones que hacen que
										este nuevo modelo.</p>
								</div>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<?php include('footer.php'); ?>
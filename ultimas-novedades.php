<?php include('header.php'); ?>

<main>
	<section class="horizon__search bg-white">
		<div class="container gridle-no-gutter">
			<div class="gridle-row">
				<div class="gridle-gr-9 gridle-gr-12@medium">
					<?php include('partials/breadcrumbs.php'); ?>
				</div>

				<div class="gridle-gr-3 gridle-gr-12@medium">
					<?php include('partials/searchbar.php'); ?>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon horizon__inner bg-white">
		<div class="container gridle-no-gutter">
			<div class="gridle-gr-12 gridle-gr-12@medium gridle-gr-centered">
				<h1 class="title">
					Últimas novedades
				</h1>

				<p class="common-box--featured">
					Estos artículos tienen una extensión máxima de texto de 300 caracteres onsectetur adipiscing elit maecenas mart sit amet
					mecenas dolor sit amet consectetur adipiscing elit maecenas element sit amet consectetur adipiscing elit maecenas mart
					sit amet mecenas dolor sit amet consectetur maecenas elemnt
				</p>
			</div>

			<div class="gridle-row">
				<div class="gridle-gr-9 gridle-gr-12@medium">
					<article class="horizon common-box--horizontal">
						<figure class="common-box__figure">
							<a href="#" title="titulo">
								<img src="http://placehold.it/240x180">
							</a>
						</figure>
						<div class="common-box__body">
							<p class="common-box__meta">Epígrafe de la noticia o artículo</p>

							<h2 class="main-title--tiny">
								<a href="#" title="titulo">Titulo de la Noticia</a>
							</h2>

							<p class="common-box__meta"><em>12 de febrero, por </em> <a href="#" title="titulo" class="font-color-grey-darkest"><strong><em> Nombre del autor </em></strong></a></p>

							<p class="common-box__excerpt font-size-small">
								Estos artículos tienen una extensión máxima de texto de 300 caracteres onsectetur adipiscing elit maecenas mart sit amet
								mecenas dolor sit amet consectetur adipiscing elit maecenas element sit amet consectetur adipiscing elit maecenas
								mart sit amet mecenas dolor sit amet consectetur maecenas elemnt
							</p>
						</div>
					</article>

					<article class="horizon common-box--horizontal">
						<figure class="common-box__figure">
							<a href="#" title="titulo">
								<img src="http://placehold.it/240x180">
							</a>
						</figure>
						<div class="common-box__body">
							<p class="common-box__meta">Epígrafe de la noticia o artículo</p>

							<h2 class="main-title--tiny">
								<a href="#" title="titulo">Titulo de la Noticia</a>
							</h2>

							<p class="common-box__meta"><em>12 de febrero, por </em> <a href="#" title="titulo" class="font-color-grey-darkest"><strong><em> Nombre del autor</em></strong></a></p>

							<p class="common-box__excerpt">
								Estos artículos tienen una extensión máxima de texto de 300 caracteres onsectetur adipiscing elit maecenas mart sit amet
								mecenas dolor sit amet consectetur adipiscing elit maecenas element sit amet consectetur adipiscing elit maecenas
								mart sit amet mecenas dolor sit amet consectetur maecenas elemnt
							</p>
						</div>
					</article>

					<article class="horizon common-box--horizontal">
						<figure class="common-box__figure">
							<a href="#" title="titulo">
								<img src="http://placehold.it/240x180">
							</a>
						</figure>
						<div class="common-box__body">
							<p class="common-box__meta">Epígrafe de la noticia o artículo</p>

							<h2 class="main-title--tiny">
								<a href="#" title="titulo">Titulo de la Noticia</a>
							</h2>

							<p class="common-box__meta"><em>12 de febrero, por </em> <a href="#" title="titulo" class="font-color-grey-darkest"><strong><em> Nombre del autor</em></strong></a></p>

							<p class="common-box__excerpt">
								Estos artículos tienen una extensión máxima de texto de 300 caracteres onsectetur adipiscing elit maecenas mart sit amet
								mecenas dolor sit amet consectetur adipiscing elit maecenas element sit amet consectetur adipiscing elit maecenas
								mart sit amet mecenas dolor sit amet consectetur maecenas elemnt
							</p>
						</div>
					</article>

					<article class="horizon common-box--horizontal">
						<figure class="common-box__figure">
							<a href="#" title="titulo">
								<img src="http://placehold.it/240x180">
							</a>
						</figure>
						<div class="common-box__body">
							<p class="common-box__meta">Epígrafe de la noticia o artículo</p>

							<h2 class="main-title--tiny">
								<a href="#" title="titulo">Titulo de la Noticia</a>
							</h2>

							<p class="common-box__meta"><em>12 de febrero, por </em> <a href="#" title="titulo" class="font-color-grey-darkest"><strong><em> Nombre del autor</em></strong></a></p>

							<p class="common-box__excerpt">
								Estos artículos tienen una extensión máxima de texto de 300 caracteres onsectetur adipiscing elit maecenas mart sit amet
								mecenas dolor sit amet consectetur adipiscing elit maecenas element sit amet consectetur adipiscing elit maecenas
								mart sit amet mecenas dolor sit amet consectetur maecenas elemnt
							</p>
						</div>
					</article>

					<article class="horizon common-box--horizontal">
						<figure class="common-box__figure">
							<a href="#" title="titulo">
								<img src="http://placehold.it/240x180">
							</a>
						</figure>
						<div class="common-box__body">
							<p class="common-box__meta">Epígrafe de la noticia o artículo</p>

							<h2 class="main-title--tiny">
								<a href="#" title="titulo">Titulo de la Noticia</a>
							</h2>

							<p class="common-box__meta"><em>12 de febrero, por </em> <a href="#" title="titulo" class="font-color-grey-darkest"><strong><em> Nombre del autor</em></strong></a></p>

							<p class="common-box__excerpt">
								Estos artículos tienen una extensión máxima de texto de 300 caracteres onsectetur adipiscing elit maecenas mart sit amet
								mecenas dolor sit amet consectetur adipiscing elit maecenas element sit amet consectetur adipiscing elit maecenas
								mart sit amet mecenas dolor sit amet consectetur maecenas elemnt
							</p>
						</div>
					</article>

					<article class="horizon common-box--horizontal">
						<figure class="common-box__figure">
							<a href="#" title="titulo">
								<img src="http://placehold.it/240x180">
							</a>
						</figure>
						<div class="common-box__body">
							<p class="common-box__meta">Epígrafe de la noticia o artículo</p>

							<h2 class="main-title--tiny">
								<a href="#" title="titulo">Titulo de la Noticia</a>
							</h2>

							<p class="common-box__meta"><em>12 de febrero, por </em> <a href="#" title="titulo" class="font-color-grey-darkest"><strong><em> Nombre del autor</em></strong></a></p>

							<p class="common-box__excerpt">
								Estos artículos tienen una extensión máxima de texto de 300 caracteres onsectetur adipiscing elit maecenas mart sit amet
								mecenas dolor sit amet consectetur adipiscing elit maecenas element sit amet consectetur adipiscing elit maecenas
								mart sit amet mecenas dolor sit amet consectetur maecenas elemnt
							</p>
						</div>
					</article>
					<div class="common-box__extra font-centered">
						<button class="button button--main button--load button--ghost">
                      			<i class="icon-elem icon-elem--loader" ></i>
                      		</button>
					</div>
				</div>
				<div class="gridle-gr-3 gridle-gr-12@medium">
					<?php include('sidebar.php'); ?>
				</div>
			</div>
		</div>
	</section>
</main>

<?php include('footer.php'); ?>
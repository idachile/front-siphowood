<h2 class="main-title">
    <a href="#" title="titulo">Productos Relacionados</a>
</h2>
<div class="gridle-row">
    <div class="gridle-gr-3 gridle-gr-12@medium">
        <article class="common-box common-box--inversed">
            <figure class="common-box__figure">
                <a href="#" title="titulo">
                    <img src="images/img/img-product-2.jpg">
                </a>
            </figure>
            <div class="common-box__body">
                <h2 class="title--tiny">
                    <a href="#" title="titulo">Producto XY</a>
                </h2>

                <p class="common-box__meta"><em>Marca</em></p>

                <p class="common-box__excerpt">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </p>

                <p class="common-box__extra">
                    <span class="common-box__tag">$4.990</span>

                    <span class="common-box__link float-right">
					    <a href="index.html" class="font-size-tiny">Inicio</a> 
						<i class="font-color_grey_lightest icon-elem icon-elem--chevron_right" ></i>
					</span>
                </p>
            </div>
        </article>
    </div>
    <div class="gridle-gr-3 gridle-gr-12@medium">
        <article class="common-box common-box--inversed">
            <figure class="common-box__figure">
                <a href="#" title="titulo">
                    <img src="images/img/img-product-8.jpg">
                </a>
            </figure>
            <div class="common-box__body">
                <h2 class="title--tiny">
                    <a href="#" title="titulo">Producto XY</a>
                </h2>

                <p class="common-box__meta"><em>Marca</em></p>

                <p class="common-box__excerpt">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </p>

                <p class="common-box__extra">
                    <span class="common-box__tag">$4.990</span>

                    <span class="common-box__link float-right">
						<a href="index.html" class="font-size-tiny">Inicio</a> 
						<i class="font-color_grey_lightest icon-elem icon-elem--chevron_right" ></i>
					</span>
                </p>
            </div>
        </article>
    </div>
    <div class="gridle-gr-3 gridle-gr-12@medium">
        <article class="common-box common-box--inversed">
            <figure class="common-box__figure">
                <a href="#" title="titulo">
                    <img src="images/img/img-product-7.jpg">
                </a>
            </figure>
            <div class="common-box__body">
                <h2 class="title--tiny">
                    <a href="#" title="titulo">Producto XY</a>
                </h2>

                <p class="common-box__meta"><em>Marca</em></p>

                <p class="common-box__excerpt">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </p>

                <p class="common-box__extra">
                    <span class="common-box__tag">$4.990</span>

                    <span class="common-box__link float-right">
						<a href="index.html" class="font-size-tiny">Inicio</a> 
						<i class="font-color_grey_lightest icon-elem icon-elem--chevron_right" ></i>
					</span>
                </p>
            </div>
        </article>
    </div>
    <div class="gridle-gr-3 gridle-gr-12@medium">
        <article class="common-box common-box--inversed">
            <figure class="common-box__figure">
                <a href="#" title="titulo">
                    <img src="images/img/img-product-4.jpg">
                </a>
            </figure>
            <div class="common-box__body">
                <h2 class="title--tiny">
                    <a href="#" title="titulo">Producto XY</a>
                </h2>

                <p class="common-box__meta"><em>Marca</em></p>

                <p class="common-box__excerpt">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </p>

                <p class="common-box__extra">
                    <span class="common-box__tag">$4.990</span>

                    <span class="common-box__link float-right">
						<a href="index.html" class="font-size-tiny">Inicio</a> 
						<i class="font-color_grey_lightest icon-elem icon-elem--chevron_right" ></i>
					</span>
                </p>
            </div>
        </article>
    </div>
</div>
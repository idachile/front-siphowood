<div class="gridle-gr-12 gridle-gr-12@medium">
	<article class="common-box--horizontal">
		<figure class="common-box__figure">
			<a href="#" title="titulo">
				<img src="images/img/img-product-4.jpg">
			</a>
		</figure>
		<div class="common-box__body">
			<p class="common-box__meta">Epígrafe de la noticia o artículo</p>

			<h2 class="main-title--tiny">
				<a href="#" title="titulo">Título en una línea</a>
			</h2>

			<p class="common-box__excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus feugiat porta efficitur. In lacinia ac arcu eu viverra.</p>
		</div>
	</article>
</div>
<div class="gridle-gr-12 gridle-gr-12@medium">
	<article class="common-box--horizontal">
		<figure class="common-box__figure">
			<a href="#" title="titulo">
				<img src="images/img/img-product-7.jpg">
			</a>
		</figure>
		<div class="common-box__body">
			<p class="font-size-tiny">
				STOCK: 100 productos
			</p>

			<h2 class="main-title--tiny">
				<a href="#" title="titulo">Título de la Noticias en dos líneas de extención</a>
			</h2>

			<p class="common-box__excerpt">Nuevo modelo de nuestro producto XY, llega a Chile el 2 de Abril, con importantes incorporaciones que hacen que este nuevo
				modelo.</p>
		</div>
	</article>
</div>
<div class="gridle-gr-12 gridle-gr-12@medium">
	<article class="common-box--horizontal">
		<figure class="common-box__figure">
			<a href="#" title="titulo">
				<img src="images/img/img-product-8.jpg">
			</a>
		</figure>
		<div class="common-box__body">
			<p class="font-size-tiny">
				STOCK: 100 productos
			</p>

			<h2 class="main-title--tiny">
				<a href="#" title="titulo">Título en línea</a>
			</h2>

			<p class="common-box__excerpt">Nuevo modelo de nuestro producto XY, llega a Chile el 2 de Abril, con importantes incorporaciones que hacen que este nuevo
				modelo.</p>
		</div>
	</article>
</div>